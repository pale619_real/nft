<?php
ini_set('max_execution_time', 1200);

/* INITIALIZE VAR */
$arrGenerated = array();
$arrAlreadyGenerated = array();
$arrDiscard = array();
$arrAlreadyDiscarded = array();
$start = time();
$j = 0;

/* DEPENDENCE */
include_once($_SERVER["DOCUMENT_ROOT"] . "/settings.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/utils.php");

/* MERGE LAYERS AND GENERATE METADATA */
if (is_array($arrGenerated) && count($arrGenerated)) {
    foreach ($arrGenerated as $key_elem => $value_option) {
        if (!array_key_exists($key_elem, $arrAlreadyImgGenerated)) {
            $attributes = array();
            $arrCollectionItem = array();
            $i = 0;
            foreach ($arrLevel as $level_name) {
                if (isset($value_option[$level_name]) && $value_option[$level_name] !== "Empty") {
                    if (!$i) {
                        $imagick = new \Imagick($_SERVER["DOCUMENT_ROOT"] . "/uploads/layers/" . $level_name . "/" . $value_option[$level_name]);
                        $i++;
                    } else {
                        ${"imagick-" . $key_elem} = new \Imagick($_SERVER["DOCUMENT_ROOT"] . "/uploads/layers/" . $level_name . "/" . $value_option[$level_name]);
                        $imagick->addImage(${"imagick-" . $key_elem});
                    }

                    $attributes[] = array(
                        "trait_type" => str_replace("-", " ", $level_name),
                        "value" => explode("#", $value_option[$level_name])[0]
                    );
                }
            }

            if ($i) {
                $arrCollectionItem = array(
                    "name" => NAME . "#" . $arrGeneratedID[$key_elem],
                    "description" => DESCRIPTION,
                    "image" => "https://" . $_SERVER["SERVER_NAME"] . "/uploads/generated/" . $key_elem . ".png",
                    "dna" => $key_elem,
                    "edition" => EDITION,
                    "date" => time(),
                    "attributes" => $attributes,
                    "compiler" => NFT_CREATOR
                );

                $sSQL = "UPDATE nft_generate SET
                                img_path = " . $db_nft->toSql("https://" . $_SERVER["SERVER_NAME"] . "/uploads/generated/" . $key_elem . ".png", "Text") . "
                                , json_path = " . $db_nft->toSql("https://" . $_SERVER["SERVER_NAME"] . "/json.php/" . $key_elem, "Text") . "
                            WHERE nft_key = " . $db_nft->toSql($key_elem, "Text");
                $db_nft->execute($sSQL);

                $filename_json = $_SERVER["DOCUMENT_ROOT"] . "/uploads/json/" . $key_elem . ".json";
                if (!is_dir(dirname($filename_json))) {
                    mkdir(dirname($filename_json), 0777, true);
                }
                file_put_contents($filename_json, json_encode($arrCollectionItem, JSON_UNESCAPED_SLASHES));

                $imagick->setImageFormat('png');

                $result = $imagick->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
                $filename = $_SERVER["DOCUMENT_ROOT"] . "/uploads/generated/" . $key_elem . ".png";
                if (!is_dir(dirname($filename))) {
                    mkdir(dirname($filename), 0777, true);
                }
                $result = file_put_contents($filename, $result->getImageBlob());
            }
            $j++;

            if ($j > 100) {
                break;
            }
        }
    }
    if($j) {
        /* GENERATE GLOBAL METADATA AND STOP SCRIPT */
        echo "ENDED " . $j . " NFT CREATED IN " . date("i:s", time() - $start) . "<br/>";
        header("Refresh:0");
    } else {
        echo "ENDED - ALL NFT ALREADY CREATED";
    }
} else {
    echo "ENDED - ALL NFT ALREADY CREATED";
}
die();