<?php
$number = basename($_SERVER["PATH_INFO"]);
if(is_file($_SERVER["DOCUMENT_ROOT"] . "/uploads/json-remote/" . $number . ".json")) {
    $string = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/uploads/json-remote/" . $number . ".json");
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json; charset=utf-8');
    echo $string;
} elseif(is_file($_SERVER["DOCUMENT_ROOT"] . "/uploads/json/" . $number . ".json")) {
    $string = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/uploads/json/" . $number . ".json");
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json; charset=utf-8');
    echo $string;
} else {
    echo "ELEMENT NOT EXISTS";
}
die();
