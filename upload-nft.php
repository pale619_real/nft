<?php
ini_set('max_execution_time', 1200);

/* INITIALIZE VAR */
$arrGenerated = array();
$arrAlreadyGenerated = array();
$arrGeneratedID = array();
$arrAlreadyRemoteImgGenerated = array();
$arrDiscard = array();
$arrAlreadyDiscarded = array();
$start = time();
$j = 0;

/* DEPENDENCE */
include_once ($_SERVER["DOCUMENT_ROOT"] . "/settings.php");
include_once ($_SERVER["DOCUMENT_ROOT"] . "/utils.php");

/* MERGE LAYERS AND GENERATE METADATA */


if(is_array($arrGenerated) && count($arrGenerated)) {
    $authorization = "Authorization: Bearer " . CLOUDFLARE_BEARER_TOKEN;
    $headers[] = $authorization;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.cloudflare.com/client/v4/accounts/" . CLOUDFLARE_ACCOUNT_ID . "/images/v1");
    curl_setopt($ch, CURLOPT_POST, 1);

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    foreach ($arrGenerated as $key_elem => $value_option) {
        if (!array_key_exists($key_elem, $arrAlreadyRemoteImgGenerated)) {
            $filename = $_SERVER["DOCUMENT_ROOT"] . "/uploads/generated/" . $key_elem . ".png";

            if(is_file($filename)) {
                $post_fields = array(
                    "file" => new \CURLFile($filename,'image/png',NAME . "#" . $arrGeneratedID[$key_elem])
                );
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
                $dataResults = json_decode(curl_exec($ch));

                if($dataResults && $dataResults->success) {
                    $attributes = array();
                    $i = 0;
                    foreach ($arrLevel as $level_name) {
                        if (isset($value_option[$level_name]) && $value_option[$level_name] !== "Empty") {
                            $attributes[] = array(
                                "trait_type" => str_replace("-", " ", $level_name),
                                "value" => explode("#", $value_option[$level_name])[0]
                            );
                        }
                    }
                    $arrCollectionItem = array(
                        "name" => $dataResults->result->filename,
                        "description" => DESCRIPTION,
                        "image" => CLOUDFLARE_DELIVERY_URL . $dataResults->result->id . "/public",
                        "dna" => $key_elem,
                        "edition" => EDITION,
                        "date" => time(),
                        "attributes" => $attributes,
                        "compiler" => NFT_CREATOR
                    );

                    $sSQL = "UPDATE nft_generate SET
                                    remote_img_path = " . $db_nft->toSql(CLOUDFLARE_DELIVERY_URL . $dataResults->result->id . "/public", "Text") . "
                                    , remote_json_path = " . $db_nft->toSql("https://" . $_SERVER["SERVER_NAME"] . "/json.php/" . $key_elem, "Text") . "
                                WHERE nft_key = " . $db_nft->toSql($key_elem, "Text");
                    $db_nft->execute($sSQL);

                    $filename_json = $_SERVER["DOCUMENT_ROOT"] . "/uploads/json-remote/" . $key_elem . ".json";
                    if (!is_dir(dirname($filename_json))) {
                        mkdir(dirname($filename_json), 0777, true);
                    }
                    file_put_contents($filename_json, json_encode($arrCollectionItem, JSON_UNESCAPED_SLASHES));

                    $j++;
                }
            }

        }
    }
    curl_close($ch);
    /* GENERATE GLOBAL METADATA AND STOP SCRIPT */
    echo "ENDED " . $j . " NFT UPLOADED IN " . date("i:s", time() - $start) . "<br/>";
    die();
} else {
    echo "ENDED - ALL NFT ALREADY UPLOADED";
    die();
}