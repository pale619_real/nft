# WELCOME TO PALE619 NFT GENERATOR

## Create generative image and their metadata using PHP.

### What tou need:
* webserver PHP 7+
* [ImageMagick](https://www.php.net/manual/en/book.imagick.php)
* An image divided into different layers to combine

### Setup
* Create asset and rename them following these rules:
  * [FILENAME]#[RARITY].[EXTENSION]
    * Filename => Name of the value of attribute
    * Rarity => Distribution of asset in percet (express in number)
    * Extension => File extension
* Insert them in a folder called as level name and put them in /uploads/layers
* Insert collection rules in setting.php (image total, collection name, description and edition)
* Insert level list (ordered from behind to front) in array $arrLevel

### Script
* Start script call /generate-nft.php

### Error
* If the sum of rarity is more than 100 system will return an error
* If the sum of rarity is less than 100 system will skip the layer for the missing percent
* If you not explicit rarity it will be considered as 100 ([FILENAME].[EXTENSION] is equal to [FILENAME]#100.[EXTENSION])

### Other info
* Assets from [Moralis NFT Generator Guide](https://moralis.io/how-to-generate-thousands-of-nfts/)


