<?php
ini_set('max_execution_time', 1200);

/* INITIALIZE ARRAY */
$arrGenerated = array();
$arrAlreadyGenerated = array();
$arrDiscard = array();
$arrAlreadyDiscarded = array();
$arrCollection = array();
$arrGeneratedList = array();
$arrLevel = array();
$arrDistributionChar = array();
$arrDistributionDistance = array();

/* DEPENDENCE */
include_once($_SERVER["DOCUMENT_ROOT"] . "/settings.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/utils.php");

/* INITIALIZE VAR */
$j = 0;
$k = 0;
$new = 0;
$distributionTotal = 0;
$missing_nft = 0;
$start = time();
$check_elem = TOTAL_NFT;
$check_round = TOTAL_TRY;
$new_elem = false;

/* CHECK TOTAL NFT */
if (count($arrGenerated) >= TOTAL_NFT) {
    foreach($arrGenerated AS $key => $value) {
        foreach ($arrLevel as $level_name) {
            if (isset($value[$level_name])) {
                $arrDistributionChar[$level_name][$value[$level_name]]++;
            } else {
                $arrDistributionChar[$level_name]["Empty"]++;
            }
        }
        $min_diff = 5;
        foreach($arrGenerated AS $key_inset => $value_inset) {
            if($key_inset != $key) {
                $result = array_diff_assoc($value_inset, $value);
                if(count($result) < $min_diff) {
                    $min_diff = count($result);
                }
            }
        }
        $arrDistributionDistance[$min_diff]++;
        $distributionTotal++;
    }
    foreach($arrDistributionChar AS $level_name => $level_value) {
        foreach($level_value AS $level_inner_name => $level_count) {
            $arrDistributionPercent[$level_name][$level_inner_name] = (100 * $level_count)/TOTAL_NFT . "%";
        }
    }
    foreach($arrDistributionDistance AS $number => $total) {
        $arrDistributionDistancePercent[$number] = (100 * $total)/$distributionTotal . "%";
    }
    print_r($arrDistributionPercent);
    die();
} else {
    $missing_nft = TOTAL_NFT - count($arrGenerated);
}
/* CHECK ASSET AND GENERATE RARITY DISTRIBUTION */
$arrChar = glob($_SERVER["DOCUMENT_ROOT"] . "/uploads/layers/*");
if (is_array($arrChar) && count($arrChar)) {
    foreach ($arrChar as $char_name) {
        $arrCharDetail = glob($_SERVER["DOCUMENT_ROOT"] . "/uploads/layers/" . basename($char_name) . "/*");
        if (is_array($arrCharDetail) && count($arrCharDetail)) {
            $arrDistribution[basename($char_name)] = array();
            $total = 0;
            foreach ($arrCharDetail as $char_detail) {
                $filename = basename($char_detail);
                $arrFilename = explode("#", explode(".", $filename)[0]);

                if (is_array($arrFilename) && count($arrFilename) === 2) {
                    $total += $arrFilename[1];

                    $arrFill = array_fill(0, ceil(TOTAL_TRY * $arrFilename[1] / 100) - $arrGeneratedList[basename($char_name)][basename($char_detail)], $filename);
                } else {
                    $total += 100;
                    $arrFill = array_fill(0, $missing_nft - $arrGeneratedList[basename($char_name)][basename($char_detail)], $filename);
                }

                $arrDistribution[basename($char_name)] = array_merge($arrDistribution[basename($char_name)], $arrFill);
            }

            if ($total > 100) {
                die("ERROR RARITY PERCENT");
            } elseif ($total < 100) {
                $arrFill = array_fill(0, ceil(TOTAL_TRY * (100 - $total) / 100) - $arrGeneratedList[basename($char_name)]["Empty"], "Empty");
                $arrDistribution[basename($char_name)] = array_merge($arrDistribution[basename($char_name)], $arrFill);

            }
            if (count($arrDistribution[basename($char_name)]) > $missing_nft) {
                shuffle($arrDistribution[basename($char_name)]);
                $arrDistribution[basename($char_name)] = array_slice($arrDistribution[basename($char_name)], 0, $missing_nft);
            }
            shuffle($arrDistribution[basename($char_name)]);
        }
    }
}

/* GENERATE UNIQUE NFT */
do {
    $k++;
    $arrNewNft = array();
    $string_key = "";
    $valid = true;
    foreach ($arrDistribution as $field_name => $field_value) {
        $arrNewNft[$field_name] = $field_value[$j];
        if (strlen($string_key)) {
            $string_key .= "-";
        }
        $string_key .= $field_value[$j];
    }
    if(strlen($string_key)) {
        $string_check = sha1(clean($string_key));
        if (array_key_exists($string_check, $arrGenerated) || array_key_exists($string_check, $arrDiscard)) {
            $valid = false;
        } elseif (NFT_DISTANCE > 1 && is_array($arrGenerated) && count($arrGenerated)) {
            foreach ($arrGenerated as $list_generated => $field_generated_value) {
                if ($valid) {
                    $result = array_diff_assoc($field_generated_value, $arrNewNft);
                    if (count($result) < NFT_DISTANCE) {
                        $valid = false;
                        $arrDiscard[$string_check] = $arrNewNft;
                    }
                }
            }
        }


        if ($valid) {
            $new_elem = true;
            $arrGenerated[$string_check] = $arrNewNft;
            $check_elem--;
        }
    }
    $check_round--;
    $j++;
} while ($check_elem > 0 && $check_round > 0);

if(is_array($arrDiscard) && count($arrDiscard)) {
    foreach ($arrDiscard as $string_check => $value) {
        if (!in_array($string_check, $arrAlreadyDiscarded)) {
            $sSQL = "INSERT INTO nft_generate_discarded
                    (
                        ID
                        , nft_key
                        , nft_value
                    ) VALUES
                    (
                        NULL 
                        , " . $db_nft->toSql($string_check, "Text") . "
                        , " . $db_nft->toSql(json_encode($value), "Text") . "
                    )";
            $db_nft->execute($sSQL);
            $arrAlreadyDiscarded[] = $string_check;
        }
    }
}

if ($new_elem) {
    foreach ($arrGenerated as $string_check => $value) {
        if (!in_array($string_check, $arrAlreadyGenerated)) {
            $sSQL = "INSERT INTO nft_generate
                        (
                            ID
                            , nft_key
                            , nft_value
                        ) VALUES
                        (
                            NULL 
                            , " . $db_nft->toSql($string_check, "Text") . "
                            , " . $db_nft->toSql(json_encode($value), "Text") . "
                        )";
            $db_nft->execute($sSQL);

            $new++;
        }
    }

    echo "ENDED " . $new . " NFT CREATED IN " . date("i:s", time() - $start) . "<br/>";
    echo "Total = " . ($new + count($arrGenerated)) . " <br/>";
    echo "TENTATIVE => " . (TOTAL_TRY - $check_round) . " - COMBINATION DISCARDED => " . count($arrDiscard);
    header("Refresh:0");
    die();
} else {
    $total_failed = $_COOKIE["failed"] +1;
    setcookie("failed", $total_failed);
    if($total_failed > 1000) {
        echo "TENTATIVE => " . (TOTAL_TRY - $check_round) . " - COMBINATION DISCARDED => " . count($arrDiscard) . "<br/>";
        echo "Impossible to create new NFT, all combination failed";
    } else {
        if(is_array($arrAlreadyDiscarded) && count($arrAlreadyDiscarded)) {
            if (is_array($arrDiscard) && count($arrDiscard)) {
                echo "TENTATIVE => " . (TOTAL_TRY - $check_round) . " - COMBINATION DISCARDED => " . count($arrDiscard);
                header("Refresh:0");
            } else {
                echo "Impossible to create new NFT, all combination failed";
            }
        } else {
            echo "TENTATIVE => " . (TOTAL_TRY - $check_round);
            header("Refresh:0");
        }
    }
    die();
}


