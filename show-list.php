<?php
ini_set('max_execution_time', 1200);
$arrListDetail = array();
include_once($_SERVER["DOCUMENT_ROOT"] . "/settings.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/utils.php");

foreach($arrListDetail AS $index => $value) {
    $img = "";
    $json = "";
    if(strlen($value["remote_img_path"]) && strlen($value["remote_json_path"])) {
        $img = $value["remote_img_path"];
        $json = $value["remote_json_path"];
    } elseif(strlen($value["img_path"]) && strlen($value["json_path"])) {
        $img = $value["img_path"];
        $json = $value["json_path"];
    }

    if(strlen($img)) {
        echo '<a href="' . $json . '" target="_blank"><img src="' . $img . '" width="100" height="100" /></a>';
    }
}
die();

