<?php
/* CHECK ALREADY CREATED */
$sSQL = "SELECT *
            FROM nft_generate
            ORDER BY ID ASC";
$db_nft->query($sSQL);
if ($db_nft->nextRecord()) {
    do {
        $arrListParams = (array) json_decode($db_nft->getField("nft_value", "Text", true));
        $arrGenerated[$db_nft->getField("nft_key", "Text", true)] = $arrListParams;
        $arrGeneratedID[$db_nft->getField("nft_key", "Text", true)] = $db_nft->getField("ID", "Number", true);

        if(strlen($db_nft->getField("img_path", "Text", true)) && is_file($_SERVER["DOCUMENT_ROOT"] . "/uploads/generated/" . $db_nft->getField("nft_key", "Text", true) . ".png")) {
            $arrAlreadyImgGenerated[$db_nft->getField("nft_key", "Text", true)] = true;
        }

        if(strlen($db_nft->getField("remote_img_path", "Text", true))) {
            $arrAlreadyRemoteImgGenerated[$db_nft->getField("nft_key", "Text", true)] = true;
        }

        if(is_array($arrListParams) && count($arrListParams)) {
            foreach($arrListParams AS $field_name => $field_value) {
                $arrGeneratedList[$field_name][$field_value]++;
            }
        }

        $arrListDetail[$db_nft->getField("nft_key", "Text", true)] = array(
            "params" => $arrListParams
            , "img_path" => $db_nft->getField("img_path", "Text", true)
            , "json_path" => $db_nft->getField("json_path", "Text", true)
            , "remote_img_path" => $db_nft->getField("remote_img_path", "Text", true)
            , "remote_json_path" => $db_nft->getField("remote_json_path", "Text", true)
        );

        $arrAlreadyGenerated[] = $db_nft->getField("nft_key", "Text", true);
    } while ($db_nft->nextRecord());
}
$sSQL = "SELECT *
            FROM nft_generate_discarded
            ORDER BY ID ASC";
$db_nft->query($sSQL);
if ($db_nft->nextRecord()) {
    do {
        $arrDiscarded[$db_nft->getField("nft_key", "Text", true)] = (array) json_decode($db_nft->getField("nft_value", "Text", true));
        $arrAlreadyDiscarded[] = $db_nft->getField("nft_key", "Text", true);
    } while ($db_nft->nextRecord());
}

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

