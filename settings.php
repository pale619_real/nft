<?php
/* COLLECTION */
define("TOTAL_NFT", 3000);
define("TOTAL_TRY", TOTAL_NFT * 2);
define("NAME", "Collection name ");
define("DESCRIPTION", "Collection description");
define("EDITION", 1);
define("NFT_DISTANCE", 2);
define("NFT_CREATOR", "pale619");

/* DATABASE */
define("NFT_DB_NAME", "");
define("NFT_DB_HOST", "");
define("NFT_DB_USER", "");
define("NFT_DB_PASSWORD", "");

define("CLOUDFLARE_BEARER_TOKEN", "");
define("CLOUDFLARE_ACCOUNT_ID", "");
define("CLOUDFLARE_DELIVERY_URL", "");

/* LIVELLI */
$arrLevel = array(
    "background"
    , "center"
    , "bottom left"
    , "top left"
    , "top right"
);

include($_SERVER["DOCUMENT_ROOT"] . "/db/ffDb_Sql_mysqli.php");
$db_nft = new ffDB_Sql();
$db_nft->connect(NFT_DB_NAME, NFT_DB_HOST, NFT_DB_USER, NFT_DB_PASSWORD);

